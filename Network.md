# scan outside of a network
## scan available networks with info
_Note: elevated privileges are necessary in order to get full access to the network card_
```
sudo iwlist scan
```

# scan inside of a network
## scan network for other devices (ping sweep & reverse dns)
```
nmap -sP 192.168.4.3/24
```

## scan host for open ports (TCP SYN)
```
nmap -sS 192.168.2.4
```

## scan host for open ports and OS
```
nmap -O 192.168.4.3
```
