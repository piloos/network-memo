# reset repository to specific commit
_from https://samwize.com/2014/01/15/how-to-remove-a-commit-that-is-already-pushed-to-github/_   
First, find the commit using 
```
git log
```
Then, reset the origin to this commit
```
git push origin +7f6d03:master
```
or 
```
git reset 7f6d03 --hard
git push origin -f
```
# Cheatsheet
[duckduckhack cheatsheet](https://duckduckgo.com/?q=git+cheatsheet&t=canonical&ia=cheatsheet&iax=cheatsheet)

| Command | Use |
| -- | -- |
| `git diff #commitFrom..#commitTo` | Compare the repository state between to specific commits |
