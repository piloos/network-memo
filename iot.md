# Mount a directory through ssh
`sshfs -o allow_other,IdentityFile=~/.ssh/id_rsa pi@cw1.local:/home/pi cw1`

# List all devices on a network which have port 22 open
`nmap 192.168.2.1/24 -p22 -oG - | awk '/22\/open/{print $2,$3}'`